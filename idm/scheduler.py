"""
Download scheduling manager.
"""

import json
import sched
import threading
import socket
from datetime import datetime
from urllib.request import Request, urlopen
import time as time_module


url_notifier = threading.Condition()


def listen_for_server():
    """
    Waits for a activation request and activates the scheduling process.
    :return: None
    """
    print('listen_for_server started')

    host = '127.0.0.1'
    port = 34567

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.bind((host, port))

        while True:
            sock.listen(1)
            (new_socket1, address1) = sock.accept()
            json_data = new_socket1.recv(4096).decode()

            if json_data != b'' and json_data is not None:

                schedule_info = json.loads(json_data)
                scheduler = sched.scheduler(time_module.time, time_module.sleep)

                threading.Thread(target=activate_sched, args=(schedule_info, scheduler)).start()


def activate_sched(schedule_info, scheduler):
    """
    Activates the scheduling process.
    :param schedule_info: json data containing the information about the schedule.
    :param scheduler: the scheduler object used to schedule the downloads.
    :return: None
    """
    exec_date = datetime(int(schedule_info['year']), int(schedule_info['month']), int(schedule_info['day']),
                         int(schedule_info['hour']), int(schedule_info['minute']))
    t = time_module.strptime(
        f'{schedule_info["year"]}-{schedule_info["month"]}-{schedule_info["day"]} {schedule_info["hour"]}:{schedule_info["minute"]}',
        '%Y-%m-%d %H:%M')
    t = time_module.mktime(t)
    print(t)
    print("scheduled download at:", exec_date)
    scheduler.enterabs(time=t, priority=1, action=downloading_proc, argument=(schedule_info['url'], schedule_info['download_path']))
    scheduler.run()


def download_file(url):
    """
    Downloads a file from a given url.
    :param url: the direct download link of the file.
    :return: the downloaded file's data.
    """
    req = Request(
        url,
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36",
            # 'Range': 'bytes=<start>'
            # 'Range': 'bytes=<start>,<end>'
            # 'Range': f'bytes={start_byte}-{end_byte}'
        },
    )
    return urlopen(req).read()


def downloading_proc(url, download_path):
    """
    Activates the download and saves it to the given path.
    :param url: the direct download link of the file.
    :param download_path: the path to which the file will be saved.
    :return: None
    """
    print('began downloading')

    download_data = download_file(url)

    with open(f'{download_path}/{url[url.rfind("/") + 1:]}', "ab") as out_file:
        out_file.write(download_data)
        print('finished downloading')


def main():
    listen_for_server()


if __name__ == '__main__':
    main()
