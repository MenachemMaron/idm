"""
Main application for IDM project.
"""

from urllib.request import Request, urlopen
from queue import Queue
import PySimpleGUI as sg
import socket
import threading
import json

url_notifier = threading.Condition()
queue = Queue()

SCHED_IP = '127.0.0.1'
SCHED_PORT = 34567


def open_main_window(json_data):
    """
    Main window of the application.
    :param json_data: the json data containing the download information.
    :return: None
    """
    sg.theme('DarkTeal10')

    layout = [
        [sg.Text('I have detected a new download, what would you like to do with it?')],
        [sg.Text(json_data['final_url'])],
        [sg.Text('Download location:'), sg.InputText('C:/'), sg.FolderBrowse()],
        [sg.Button('Download'), sg.Button('Schedule'), sg.Button('Cancel')],
    ]

    window = sg.Window('Window Title', layout, element_justification='c')
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Cancel':
            break
        elif event == 'Download':
            downloading_proc(json_data, values[0])
        elif event == 'Schedule':
            open_scheduling_window(json_data, values[0])

    window.close()


def open_scheduling_window(json_data, download_path):
    """
    Contains the scheduling window which is activated when a user attempts to schedule a download.
    :param json_data: the json data containing the download information.
    :param download_path: the path to which the file will be saved.
    :return: None
    """
    sg.theme('DarkTeal10')

    layout = [
        [sg.Text('Schedule a download')],
        [sg.Text('Year:'), sg.InputText()],
        [sg.Text('Month:'), sg.InputText()],
        [sg.Text('Day:'), sg.InputText()],
        [sg.Text('Hour:'), sg.InputText()],
        [sg.Text('Minute:'), sg.InputText()],
        [sg.Button('Submit'), sg.Button('Cancel')]
    ]

    window = sg.Window('Window Title', layout, element_justification='c')
    # Event Loop to process "events" and get the "values" of the inputs
    while True:
        event, values = window.read()
        if event == sg.WIN_CLOSED or event == 'Cancel':
            break
        elif event == 'Submit':
            schedule_download_proc(values[0], values[1], values[2], values[3], values[4], json_data['final_url'], download_path)
            break

    window.close()


def schedule_download_proc(year, month, day, hour, minute, url, download_path):
    """
    Sends the download information to the scheduler and by doing so activates it.
    :param year: the year in which the download will be scheduled.
    :param month: the month in which the download will be scheduled.
    :param day: the day in which the download will be scheduled.
    :param hour: the hour in which the download will be scheduled.
    :param minute: the minute in which the download will be scheduled.
    :param url: the direct download link of the file.
    :param download_path: the path to which the file will be saved.
    :return: None
    """
    print('scheduling download')
    schedule_json = {'year': year, 'month': month, 'day': day, 'hour': hour, 'minute': minute, 'url': url, 'download_path': download_path}
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as send_sock:
        send_sock.connect((SCHED_IP, SCHED_PORT))
        send_sock.sendall(json.dumps(schedule_json).encode())


def download_file(input_url):
    """
    Downloads a file from a given url.
    :param input_url: the direct download link of the file.
    :return: the downloaded file's data.
    """
    url = input_url
    req = Request(
        url,
        headers={
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.45 Safari/537.36",
            # 'Range': 'bytes=<start>'
            # 'Range': 'bytes=<start>,<end>'
            # 'Range': f'bytes={start_byte}-{end_byte}'
        },
    )
    return urlopen(req).read()


def downloading_proc(json_data, folder_path):
    """
    Activates the download and saves it to the given path.
    :param json_data: the json data containing the download information.
    :param folder_path: the path to which the file will be saved.
    :return: None
    """
    url = json_data['final_url']
    download_data = download_file(url)

    with open(f'{folder_path}/{url[url.rfind("/") + 1:]}', "ab") as out_file:
        out_file.write(download_data)


def listen_for_server():
    """
    Waits for the server to notify and alerts the main application.
    :return: None
    """
    print('listen_for_server started')

    host = '127.0.0.1'
    port = 23456
    global global_json_data

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.bind((host, port))

        while True:
            sock.listen(1)
            (new_socket1, address1) = sock.accept()

            json_data = new_socket1.recv(4096).decode()
            if json_data != b'' and json_data is not None:
                print(json.loads(json_data))
                global_json_data = json.loads(json_data)
                url_notifier.acquire()
                url_notifier.notify()
                url_notifier.release()
                print('listen_for_server notified')


def main_proc():
    """
    Opens the main window and waits for the user to submit a download request.
    :return: None
    """
    print('main_proc started')
    while True:
        url_notifier.acquire()
        url_notifier.wait()
        print('main_proc notified')
        url_notifier.release()

        open_main_window(global_json_data)


def main():
    threading.Thread(target=listen_for_server).start()
    threading.Thread(target=main_proc).start()


if __name__ == "__main__":
    main()
