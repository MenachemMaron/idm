@echo off
set /p PYTHONPATH="Please enter your Python's folder path: "
echo %PYTHONPATH%
%PYTHONPATH%\python.exe -m pip install --upgrade pip
%PYTHONPATH%\scripts\pip.exe install -r requirements.txt
start /MIN cmd.exe /c %PYTHONPATH%\python.exe listening.py
start /MIN cmd.exe /c %PYTHONPATH%\python.exe main_app.py
start /MIN cmd.exe /c %PYTHONPATH%\python.exe scheduler.py