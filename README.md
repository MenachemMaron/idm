## Installation instructions:

**Notice:** the program works on Windows devices only!

You must have Python 3.7 or above installed.
If you do not have Python installed you can get it [here](https://www.python.org/downloads/).

----

### Installation steps:

1. First you must head to [this link](https://drive.google.com/file/d/1o8y29GLr1v-S3cMKLO6qClin7JsnRzVr/view?usp=sharing) and download the file.

2. Next you must extract the file to your selected path and open it.

3. Open Chrome and enter chrome://extensions.

4. In the newly opened site you must enable Developer Mode, which is located in the top right of the site.

5. Next you must select "Load Unpacked", which is located in the top left of the site, and select the folder named "chrome_extension" which is included in the extracted folder. Make sure the extension is indeed enabled.

6. Re-enter the extracted folder and run "run.bat".

7. In the freshly opened CMD window enter the path to your Python folder. If you do not know where it is located you can follow [this guide](https://datatofish.com/locate-python-windows/).

----

The program is now installed and all you need to do is begin a Chrome download.

----

In order to disable the program you must close the opened CMD windows and disable the Chrome extension.
